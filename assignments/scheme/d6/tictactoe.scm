;; scheme tictactoe homework
;; name: ??????????
;; date: ??????????

(load "./paradigms_ttt.scm") ; this file includes the tree structure and some other utility functions
(use-modules (ice-9 paradigms_ttt))

(load "./d6.scm"); this line will include your solution
(use-modules (ice-9 myd6))

;; notice the currying function cf
(define cf (lambda (gs) (value 'x gs)))


(define sum*-g
  (lambda (ttup f)
    (cond
      ((null? ttup) '0)
      ((null? (cdr ttup)) (f (car ttup)))
      (else (+ (sum*-g (car (cdr ttup)) f) (sum*-g (cons (car ttup) (cdr (cdr ttup))) f))))))

;; returns the nodecrawl* value for every game tree in the input list
(define vallist
  (lambda (p l)
    (cond
      ((null? l) (list))
      (else (cons (sum*-g (car l) cf) (vallist p (cdr l)))))))
;; notice how the currying function cf is being used in the recursive call to sum*-g

;; given the game tree (where the current situation is at the root),
;; return a recommendation for the next move
(define nextmove
  (lambda (p gt)
    (pick (positionof (greatest (vallist p (cdr gt))) (vallist p (cdr gt))) (firsts (cdr gt)))))

(display "Test for tictactoe\n")
;; what is the current game situation?
(display "Current State:     ")
(display (car (onegametree)))
(display "\n")

;; test of nextmove, where should we go next?
(display "Recommended Move:  ")
(display (nextmove 'x (onegametree)))
(display "\n")

;(display (sum*-g onegametree cf))
;(display "\n")

;; correct output for just the tic tac toe part:
;;   Current State:     (x o x o o e e x e)
;;   Recommended Move:  (x o x o o x e x e)

