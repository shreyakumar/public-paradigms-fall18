
class _movie_database:

       def __init__(self):
         self.movies = list()

       def load_movies(self, movie_file):
        f = open(movie_file)
        for line in f:
                line = line.rstrip()
                components = line.split("::")
                mid = int(components[0])
                mname = components[1]
                mgenres = components[2]

                self.movies.append(mname)
        f.close()

       def print_sorted_movies(self):
        self.movies = sorted(self.movies)
        for movie in self.movies:
          print(movie)

if __name__ == "__main__":
       mdb = _movie_database()

       #### MOVIES ########
       mdb.load_movies('ml-1m/movies.dat')
       mdb.print_sorted_movies()

