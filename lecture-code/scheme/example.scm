(define genericlat
  (lambda (seq a lat)
   (cond
    ((null? lat) '())
    ((eq? a (car lat))(seq (genericlat seq a (cdr lat))) )
    (else (cons (car lat) (genericlat seq a (cdr lat))))
    )))

(define seqrepl
  (lambda (l)
    (cons 'morebacon l)
    ))



(define seqrem
  (lambda (l)
    l))

(display (genericlat seqrepl 'turkey '(bacon cheese turkey)))

(display (genericlat seqrem  'turkey '(bacon cheese turkey)) )
