import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class Form(QDialog):
    
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        #adding a visual button
        self.exitbutton = QPushButton("Exit")

        layout = QHBoxLayout() # layout is not a member
        layout.addWidget(self.exitbutton)

        self.setLayout(layout)

        # adding functionality for button
        #              agent                event           event handler/SLOT
        self.connect(self.exitbutton, SIGNAL("clicked()"), self.exit_program)


    def exit_program(self):
        app.quit()



app = QApplication(sys.argv)

form = Form() # empty widget

#QTimer.singleShot(5000, app.quit)
form.show() #this does NOT show the form. creates an event

app.exec_() # starts the event queue
