import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

app = QApplication(sys.argv)

form = QDialog() # empty widget

QTimer.singleShot(5000, app.quit)
form.show() #this does NOT show the form. creates an event

app.exec_() # starts the event queue
