import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

class MyGUI(QMainWindow):

    def __init__(self):
        super(MyGUI, self).__init__()
        self.central = Form(parent=self)
        self.setCentralWidget(self.central)

        self.filemenu = self.menuBar().addMenu("File")
        fileExitAction = QAction("Exit", self)
        self.filemenu.addAction(fileExitAction)

        self.connect(fileExitAction, SIGNAL("triggered()"), self.exit_program)

    def exit_program(self):
        app.quit()


class Form(QWidget):
    
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        #displaying images
        self.image = QImage('/home/paradigms/images/zzjwkKxcRWoW1K0FT8TOpjeVZSL.jpg')
        self.imageLabel = QLabel('no image available')

        self.imageLabel.setAlignment(Qt.AlignCenter)
        self.imageLabel.setPixmap(QPixmap.fromImage(self.image))
        
        #adding a visual button
        self.exitbutton = QPushButton("Exit")

        layout = QHBoxLayout() # layout is not a member
        layout.addWidget(self.exitbutton)
        layout.addWidget(self.imageLabel)

        self.setLayout(layout)

        # adding functionality for button
        #              agent                event           event handler/SLOT
        self.connect(self.exitbutton, SIGNAL("clicked()"), self.exit_program)


    def exit_program(self):
        app.quit()



app = QApplication(sys.argv)

form = MyGUI() # empty widget

#QTimer.singleShot(5000, app.quit)
form.show() #this does NOT show the form. creates an event

app.exec_() # starts the event queue
