import java.util.Scanner;
import java.io.File;

public class FileReadDemo {
    public static void main(String[] args) throws Exception{
        // create a file instance
        File myFile = new File("scores.txt");

        // create a scanner instance using the file instance
        Scanner in = new Scanner(myFile);
        // Scanner in = new Scanner(new File("scores.txt"));

        // read data from scanner object
        while(in.hasNext()){
            String firstName = in.next();
            String lastName = in.next();
            int score = in.nextInt();
            System.out.println(firstName + " " + lastName + " scored a " + score);
        }   // end of while
        in.close(); // close scanner handle

        //another while
        Scanner in2 = new Scanner(myFile);
        //read file contents into array
        while(in2.hasNext()){
            String line = in2.nextLine();
            String[] tokens = line.split(" ");
            for (String token : tokens){
                System.out.println(token + ", ");
                if (token.matches("Dwight"))
                    System.out.println(" Beets ");
            }
        }   // end of while
        in2.close();
    }   // end of main
}
