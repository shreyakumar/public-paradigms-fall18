import java.util.Scanner;
import java.io.*;

public class BuffFileReadDemo {
    public static void main(String[] args) throws Exception{
        // create a file instance
        File myFile = new File("scores.txt");
        BufferedReader br = null;

        try{
            FileInputStream fis = new FileInputStream(myFile);
            InputStreamReader isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            //BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("scores.txt"))));
        }
        catch(IOException e){
            e.printStackTrace();
        }

        String line;
        while((line = br.readLine()) != null){
            System.out.println(line);
        }   // end of while
        br.close(); 

    }   // end of main
}
