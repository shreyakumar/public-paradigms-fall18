import java.util.Scanner;

public class Quotient{
    public static int calculateQuotient(int num1, int num2){
        if (num2 == 0)
            throw new ArithmeticException("new exception in method");
        return num1/num2;
    } // end of method

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter 2 integers for division: ");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        try {
            int result = calculateQuotient(number1, number2);
            System.out.println("Result = " + result);
        }
        catch(ArithmeticException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.out.println("Exception occured because you are a bad person.");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{ // if try is started, this block will be executed.
            
        }

        System.out.println("Execution continues ... ");

    }   // end of main
}
