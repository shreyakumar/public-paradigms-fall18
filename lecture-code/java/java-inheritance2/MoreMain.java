// correct example constructors and object creation
import java.util.*;

class Person{

    // properties
    String netid;
    private String firstName;
    String lastName;
    Date joiningDate;
    String classification; // freshman, sophomore; tenured, emeritus

    //Methods
    public Person(String fname){
        //super(); // has to be the first line, or not present here.
        this.firstName = fname; // this cannot be the first line - error!
	    System.out.print("P cons --> ");
    }

    String findFood(){
        return "go to Duncan";
    }

} // end of class Person

class Student extends Person{

    double gpa;

    //methods

    //constructor
    public Student(){
	    this("Default Stu");
	    System.out.print("S cons --> ");
    }

    public Student(String fname){
        super(fname);
		System.out.print("S argcons --> ");
    }


    double calculateFinancialAid(){
            return 500000.0;
    }

}

class Faculty extends Person{

    // properties
    String officeHours;

    //methods
    public Faculty(String fname){
        super(fname);
    }

    double calculateSalary(){
            return 1000000.0;
    }

}   // end of class Faculty

public class MoreMain{

    public static void main(String[] args){

        // Person[] p = new Person[3];
        //
        // p[0] = new Student();
        // Faculty f = new Faculty();
        // p[1] = f;
        //
        // System.out.println("F's Salary: " + f.calculateSalary());
        // System.out.println("p1's Salary: " + ((Faculty)p[1]).calculateSalary());
        // System.out.println("p0's Fin Aid: " + ((Student)p[0]).calculateFinancialAid());
        // System.out.println("p0 Find food: " + p[0].findFood());
        // System.out.println("p1 Find food: " + p[1].findFood());


        Student s = new Student();
        // P cons --> S argcons --> S cons
    }
}
