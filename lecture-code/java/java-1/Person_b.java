import java.util.*;

public class Person_b{

    // properties
    boolean isStudent;
    String netid;
    String firstName;
    String lastName;
    Date joiningDate;
    double gpa;
    String classification; // freshman, sophomore; tenured, emeritus


    // methods
    double calculateSalary(){
        if (isStudent) {return 0; }
        else {
            return 100000000.0;
        }
    }

    double calculateFinancialAid(){
        if (!isStudent) { return 0; }
        else {
            return 50000000;
        }
    }

    String findFood(){
        return "go to Duncan Center";
    }

    public static void main(String[] args){
        Person_b p = new Person_b();

        System.out.println("Salary: "+ p.calculateSalary());
        System.out.println("Financial Aid is " + p.calculateFinancialAid());
    }

}
