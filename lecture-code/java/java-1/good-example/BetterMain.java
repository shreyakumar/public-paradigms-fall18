import java.util.*;

class Person{  // Parent class

    // properties
    String netid;
    String firstName;
    String lastName;
    Date joiningDate;
    String classification; // freshman, sophomore; tenured, emeritus


    // methods

    String findFood(){
        return "go to Duncan Center";
    }


} // end of Person

class Student extends Person{ // child of Person

    // properties
    double gpa;


    // methods

    double calculateFinancialAid(){
            return 50000000;
    }

} // end of Student

class Faculty extends Person{ // child of Person


    // properties


    // methods
    double calculateSalary(){
            return 100000000.0;
    }

} // end of Faculty

public class BetterMain{

    public static void main(String[] args){
        Student s = new Student();
        Faculty f = new Faculty();

        Person ps = new Student();

        Person[] p_arr = new Person[3];

        p_arr[0] = new Student();

        Faculty fp = new Faculty();
        p_arr[1] = fp;


        System.out.println("Salary: "+ f.calculateSalary());
        System.out.println("Fin Aid: " + s.calculateFinancialAid());
        System.out.println("Food? " + s.findFood());

        System.out.println("Salary : " + fp.calculateSalary());
        //System.out.println("Salary : " + p_arr[1].calculateSalary());        
    }
}
