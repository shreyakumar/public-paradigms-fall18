import java.util.*;

class Student{

    // properties
    String netid;
    String firstName;
    String lastName;
    Date joiningDate;
    double gpa;
    String classification; // freshman, sophomore; tenured, emeritus


    // methods

    double calculateFinancialAid(){
            return 50000000;
    }

    String findFood(){
        return "go to Duncan Center";
    }


} // end of Student

class Faculty{

    // properties
    String netid;
    String firstName;
    String lastName;
    Date joiningDate;
    double gpa;
    String classification; // freshman, sophomore; tenured, emeritus


    // methods
    double calculateSalary(){
            return 100000000.0;
    }


    String findFood(){
        return "go to Duncan Center";
    }


} // end of Faculty

public class BadMain{

    public static void main(String[] args){
        Student s = new Student();
        Faculty f = new Faculty();

        System.out.println("Salary: "+ f.calculateSalary());
        System.out.println("Fin Aid: " + s.calculateFinancialAid());
        System.out.println("Food? " + s.findFood());        
    }
}
