import requests
import json

class _webservice_primer:
    def __init__(self):
        self.SITE_URL = 'http://student04.cse.nd.edu:51001' # this is the class webservice
        self.MOVIES_URL = self.SITE_URL + '/movies/'
        self.RESET_URL  = self.SITE_URL + '/reset/'

    def get_movie(self, mid):
        url = self.MOVIES_URL + str(mid)
        r = requests.get(url)
        resp = json.loads(r.content)
        return(resp)

    def reset_movie(self, mid):
        url = self.RESET_URL + str(mid)
        mydata = {}
        r = requests.put(url, data = json.dumps(mydata))
        resp = json.loads(r.content)
        return(resp)

if __name__ == "__main__":
    MID = 32 # change this to yours
    ws = _webservice_primer()
    print(ws.get_movie(MID))
    ws.reset_movie(MID)
    print(ws.get_movie(MID))
