#code
import cherrypy
import json
from dictionary_controller import DictionaryController # getting our class

def start_service():

    #object
    dictionaryController = DictionaryController()
    #create a dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    #connect
    dispatcher.connect('dict_get_key', '/dictionary/:key', controller=dictionaryController, action='GET_KEY', conditions=dict(method=['GET']))
    dispatcher.connect('dict_put_key', '/dictionary/:key', controller=dictionaryController, action='PUT_KEY', conditions=dict(method=['PUT']))

    #configuration for server
    conf = {
            'global' : {
                    'server.socket_host' : 'student04.cse.nd.edu',
                    'server.socket_port' : 51005,
                },
            '/' : { 'request.dispatch' : dispatcher },
            }
    #update config
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


if __name__ == '__main__':
    start_service()
