import json

class DictionaryController(object):
    def __init__(self):
        self.myd = dict()

    def get_value(self, key):
        return self.myd[key]

    # event handlers for resource requests
    def GET_KEY(self, key):
        #rule 1
        output = {'result' : 'success'}

        #rule 2 - check your data - key or payload
        key = str(key)
        
        #rule 3 - try - except
        try:
            value = self.get_value(key)
            if value is not None:
                output['key'] = key
                output['value'] = value
            else:
                output['result'] = 'error'
                output['message'] = 'None type value associated with requested key'
        except KeyError as ex:
            output['result'] = 'error'
            output['message'] = 'key not found'
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)


    def PUT_KEY(self, key):
        output = {'result': 'success'}

        key = str(key)
        #extract message from body
        data = json.loads(cherrypy.request.body.read())

        try:
            val = data['value']
            self.myd[key] = val # do your local data update
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)






        
