#code
import cherrypy
import json

class MyController(object):

    def GET_CONDITION(self, condition): #event handler for GET
        my_dictionary = {"result" : "success", condition : "boo!"}
        return json.dumps(my_dictionary)

    def PUT_CONDITION(self, condition):
        #extract payload from the body of the HTTP request
        payload = cherrypy.request.body.read()
        print(payload)
        return payload

def start_service():

    mycon = MyController() #object

    #create a dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    #connect
    dispatcher.connect('scary_get_w_condition', '/halloween/:condition', controller=mycon, action='GET_CONDITION', conditions=dict(method=['GET']) )

    dispatcher.connect('scary_put_w_condition', '/halloween/:condition', controller=mycon, action='PUT_CONDITION', conditions=dict(method=['PUT']))

    #configuration for server
    conf = {
            'global' : {
                    'server.socket_host' : 'student04.cse.nd.edu',
                    'server.socket_port' : 51005,
                },
            '/' : { 'request.dispatch' : dispatcher },
            }
    #update config
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


if __name__ == '__main__':
    start_service()
