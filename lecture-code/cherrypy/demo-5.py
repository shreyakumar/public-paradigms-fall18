import cherrypy
import json

class MyController(object):

    def GET_CONDITION(self, condition):
        my_dictionary = {"result" : "success", condition : "boo!"}
        return json.dumps(my_dictionary)

    def PUT_CONDITION(self, condition):
        #extract body of put message
        payload = cherrypy.request.body.read()
        print(payload)
        # this is a good place for me to update my stored data
        return json.dumps({"result":"success", condition : json.loads(payload)})



def start_service():
    # object of controller
    mycon = MyController()

    # create dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()
    dispatcher.connect('scary_get_with_condition', '/halloween/:condition', controller=mycon, action='GET_CONDITION', conditions=dict(method=['GET']))

    dispatcher.connect('scary_put_with_condition', '/halloween/:condition', controller=mycon, action='PUT_CONDITION', conditions=dict(method=['PUT']))

    #configuration for server
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port' : 51005,
                },
            '/' : { 'request.dispatch' : dispatcher },
            }

    #update config
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    start_service()

