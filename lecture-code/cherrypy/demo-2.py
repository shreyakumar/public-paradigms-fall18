import cherrypy

class MyController(object):

    def GET_SIMPLE(self):
        return "boo!"



def start_service():
    # object of controller
    mycon = MyController()

    # create dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()
    dispatcher.connect('scary_connection', '/halloween/', controller=mycon, action='GET_SIMPLE', conditions=dict(method=['GET']))


    #configuration for server
    conf = {
            'global' : {
                'server.socket_host' : 'student04.cse.nd.edu',
                'server.socket_port' : 51005,
                },
            '/' : { 'request.dispatch' : dispatcher },
            }

    #update config
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

if __name__ == '__main__':
    start_service()

