package main

import (
    "fmt"
    "net"
    "bufio"
)

func main() {

    // make connection
    conn, err := net.Dial("tcp", "student04.cse.nd.edu:51002")
    // send query
    fmt.Fprintln(conn, "GET /movies/32 HTTP/1.0 \r\n\r\n")

    // create a reader
    reader := bufio.NewReader(conn)
    line, err := reader.ReadString('}')

    // print results
    fmt.Println(line)
    fmt.Println(err)
}
