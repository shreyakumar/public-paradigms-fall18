package main

import (
    "fmt"
    "time"
)

func repeater(c chan string){

    msg := <-c
    msg = msg + " repeated"
    c <- msg
}

func single(c chan string){
    out := "paradigms"
    c <- out
}

func main() {
    c := make(chan string, 1)
    go repeater(c)
    go single(c)
    
    time.Sleep(100000)
    a := <- c
    fmt.Println(a)
    time.Sleep(100000)
}
