package main

import (
    "fmt"
    "net"
    "bufio"
)

func main(){
    // making a connection
    conn, err := net.Dial("tcp", "student04.cse.nd.edu:51002")
    // making a request
    fmt.Fprintln(conn, "GET /movies/32 HTTP/1.0 \r\n\r\n")

    // create a reader
    reader := bufio.NewReader(conn)
    line, err := reader.ReadString('}')

    // print result
    fmt.Println(line)
    fmt.Println(err)

}
