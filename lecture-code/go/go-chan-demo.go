package main

import (
    "fmt"
    "time"
)

func single(c chan string){
    out := "paradigms"
    c <- out
}

func repeater(c chan string){
    msg := <- c
    msg = msg + "repeater"
    c <- msg
}


func main() {
    c := make(chan string, 1)

    //go single(c)
    go repeater(c)
    go single(c)

    time.Sleep(10000)
    a:= <- c
    //time.Sleep(10000)
    fmt.Println(a)

    //time.Sleep(10000)

}
