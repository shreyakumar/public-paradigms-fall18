package com.example.shreyakumar.fall18_lecture_android1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mStudentListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // R is the class that has all the resources information.

        mStudentListView = (TextView) findViewById(R.id.tv_student_names); // mStudentListView property is connected to view system's activity_main's tv_student_name

        mStudentListView.append("\n\n\n");

        String[] studentNames = {"Morgan", "Emmet", "Henri"};

        for (String studentName : studentNames){
            mStudentListView.append(studentName + "\n\n\n");
        }


    }
}
