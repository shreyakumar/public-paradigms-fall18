package com.example.shreyakumar.sk_fall18_practice.utilities;

import android.net.Uri;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.Scanner;
import java.io.InputStream;
import java.io.IOException;
import android.util.Log;

/**
 * Created by shreyakumar on 10/5/18.
 */

public class NetworkUtils {
    static final String REDDIT_URL = "https://www.reddit.com/r/";
    static final String endformat = ".json";

    public static URL buildUrl(String userQuery){
        Uri builtUri = Uri.parse(REDDIT_URL).buildUpon()
                .appendPath(userQuery)
                .appendPath(endformat)
                .build();

        URL url = null;
        try{
            url = new URL(builtUri.toString());
            Log.d("informational", "URL: " + builtUri.toString());

        }catch (MalformedURLException e){
            e.printStackTrace();
        }
        return url;
    }   // end of method buildUrl

    public static String getResponseFromHttpUrl(URL url) throws IOException{
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) return scanner.next();
            else return null;
        } finally {
            urlConnection.disconnect();
        }
    }   // end of method
} // end of class
