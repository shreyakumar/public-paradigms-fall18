// updated mainActivity.java
package com.example.shreyakumar.sk_fall18_practice;

// new imports
import com.example.shreyakumar.sk_fall18_practice.utilities.NetworkUtils;
import android.content.Intent;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;
import android.view.View;
import android.content.Context;
import android.widget.Toast;
import android.view.Menu;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

//import android.util.Log;

public class MainActivity extends AppCompatActivity {

    // TODO 4 create handles that will connect with visual elements from View system
    private TextView mSearchResultsTextView;  //handle to connect with the visual element text view
    private TextView mDisplayTextView;
    private EditText mSearchBoxEditText;
    private Button   mSearchButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // connects the content associated with this activity to view elements created by activity_main

        // TODO 5 connecting class properties to view system elements
        mSearchBoxEditText      = (EditText) findViewById(R.id.et_search_box);
        mDisplayTextView        = (TextView) findViewById(R.id.tv_display_text);
        mSearchResultsTextView  = (TextView) findViewById(R.id.tv_results);

        mSearchButton           = (Button) findViewById(R.id.button_search);

        // TODO 6 connecting listener to button
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { // closure, therefore onClick has access to onCreate's context
                // Code here executes on main thread after user presses button
                Log.d("informational","Search button selected");

                Context c = MainActivity.this;
                String msg = "Search clicked!";
                Toast.makeText(c, msg, Toast.LENGTH_LONG).show();

                makeSearchQuery();
            }
        });

    } // end of onCreate

    // TODO Function makeSearchQuery
    private void makeSearchQuery(){
        String searchQuery = mSearchBoxEditText.getText().toString();
        mDisplayTextView.setText("Results for " + searchQuery);
        mSearchResultsTextView.setText(""); // empty string

        new FetchNetworkData().execute(searchQuery);

    } // end of method makeSearchQuery

    // TODO new inner async class to do networking
    public class FetchNetworkData extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params){
            if (params.length == 0) return null;
            String searchQuery = params[0]; // this will receive whatever is passed to execute

            URL url = NetworkUtils.buildUrl(searchQuery);

            String responseString = null;
            try{
                responseString = NetworkUtils.getResponseFromHttpUrl(url);
            } catch(Exception e){
                e.printStackTrace();
            }
            return responseString;
        }   // end of method doInBackground

        // TODO add onPostExecute
        @Override
        protected void onPostExecute(String responseData){
            String [] titles = processRedditJson(responseData);
            for (String title : titles){
                mSearchResultsTextView.append(title + "\n\n");
            }
        }   // end onPostExecute method

        public String[] processRedditJson(String responseJsonData){
            String[] newsTitles = new String[25];
            try{
                JSONObject allNewsReddit = new JSONObject(responseJsonData);
                JSONObject allNewsObject = allNewsReddit.getJSONObject("data");
                JSONArray children = allNewsObject.getJSONArray("children");
                newsTitles = new String[children.length()];
                for (int i = 0; i < newsTitles.length; i++){
                    JSONObject childJson = children.getJSONObject(i);
                    JSONObject childData = childJson.getJSONObject("data");
                    String title = childData.getString("title");

                    newsTitles[i] = title;
                }   // end of for
            }catch(JSONException e){
                e.printStackTrace();
            }
            return newsTitles;
        }   // end of method processRedditJson


    }   // end of class FetchNetworkData


    // TODO after res/menu/main/xml created and res/values/strings.xml updatedMenu inflating code
    @Override
    public boolean onCreateOptionsMenu(Menu menu){ // create the menu
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }   // end of onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item){ // do something when item is selected.
        int menuItemSelected = item.getItemId();

        if (menuItemSelected == R.id.action_about){

            // TODO grab new message
            //String msg = "news";
            String msg = mSearchBoxEditText.getText().toString();
            //Toast.makeText(c, msg, Toast.LENGTH_LONG).show();
            Log.d("informational", msg);

            // TODO call About page
            // create intent and call AboutActivity
            Context c = MainActivity.this;
            Class destinationActivity = AboutActivity.class;

            //intent creation
            Intent startAboutActivityIntent = new Intent(c, destinationActivity);

            startAboutActivityIntent.putExtra(Intent.EXTRA_TEXT, msg);

            startActivity(startAboutActivityIntent);
            Log.d("informational", "About activity launched!");
        }
        return true;
    }   // end of onOptionsItemSelected
} // end of class MainActivity
