// new file2: AboutActivity.java
package com.example.shreyakumar.sk_fall18_practice;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.shreyakumar.sk_fall18_practice.utilities.NetworkUtils;

import java.net.URL;

/**
 * Created by shreyakumar on 10/7/18.
 */

public class AboutActivity extends AppCompatActivity {

    private TextView    mDisplayAboutTextView;
    private Button      mOpenWebpageButton;
    private Button      mOpenMapButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mOpenWebpageButton  = (Button) findViewById(R.id.button_open_webpage);
        mOpenMapButton      = (Button) findViewById(R.id.button_open_map);

        mDisplayAboutTextView = (TextView) findViewById(R.id.tv_about_display_text);
        mDisplayAboutTextView.setText("Changed!");

        // grabbing the data that the intent sends us
        Intent intentThatStartedThisActivity = getIntent();
        String message = "news";
        //check is extra data
        if(intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)){
            message = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
            mDisplayAboutTextView.append("\n\n\n" + message);
        } // end if

        // react to open webpage button
        final String userQueryString = message;

        // add buttonlistener
        mOpenWebpageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                URL url = NetworkUtils.buildUrl(userQueryString);
                Log.d("informational", userQueryString + " " + url.toString());
                openWebPage(url.toString());

            } // end of onClick
        }); // end of setOnClickListener

        // react to open map button
        mOpenMapButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                openMap();

            }   // end of onClick
        }); // end of setOnClickListener

    } // end onCreate

    // method to open map
    public void openMap(){
        // create a Uri for maps
        String addressString = "University of Notre Dame, Notre Dame, IN";

        //Uri.Builder builder = new Uri.Builder();
        //builder.scheme("geo").path("0, 0").query(addressString);
        //Uri addressUri = builder.build();

        Uri addressUri = Uri.parse("geo:0,0?").buildUpon().appendQueryParameter("q", addressString).build();

        Log.d("addressUri", addressUri.toString());

        Intent openMapIntent = new Intent(Intent.ACTION_VIEW);
        openMapIntent.setData(addressUri);

        if (openMapIntent.resolveActivity(getPackageManager()) != null){
            startActivity(openMapIntent);
        }   // end if

    }   // end openMap

    // method to launch the webpage
    public void openWebPage(String urlString){
        Uri webpage = Uri.parse(urlString);
        Intent openWebpageIntent = new Intent(Intent.ACTION_VIEW, webpage);
        // check if this intent can be launched
        if (openWebpageIntent.resolveActivity(getPackageManager()) != null ){
            startActivity(openWebpageIntent);
        }   // end if


    }   // end openWebPage

} // end class
