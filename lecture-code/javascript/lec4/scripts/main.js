// JS lecture 4

var bird = {
    fly : function(){
        console.log("I'm flying");
        alert("I'm flying");
    }
}

var duck2 = {
    quack2 : function(){
        alert("quack 2!");
    }
}

function quack() {
    console.log("outside quack");
}

function duck() {
    this.quack = function(){
        alert("inside duck quack!");
    }
}

var x = 10;
console.log(this.x);
//this.quack(); // call 1

duck(); // 'this' becomes duck's 'this'

//this.quack(); // call 2
console.log(this.x);

duck.prototype = bird; // another way to use prototypical inheritance

duck.prototype = new Animal(); // yet another way

var don = new duck();
//don.quack();
//don.fly();

----
