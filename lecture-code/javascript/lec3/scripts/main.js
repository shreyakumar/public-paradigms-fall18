// JS lecture 3

var bird = {
    fly : function(){
        console.log("I'm flying");
        alert("I'm flying");
    }
}

var duck2 = {
    quack2 : function(){
        alert("quack 2!");
    }
}

function quack() {
    console.log("outside quack");
}

function duck() {
    this.quack = function(){
        alert("inside duck quack!");
    }
}

var x = 10;
console.log(this.x);
this.quack(); // call 1

duck(); // 'this' becomes duck's 'this'

this.quack(); // call 2
console.log(this.x);

// ------------

this.dog = "rover"
console.log(this.dog)

function testDog() {
    this.dog = "felix"
    console.log(this.dog)
}

console.log(this.dog)
testDog()
console.log(this.dog)

// -----------

// how to make network connections - client side

// sequence of events

var xhr = new XMLHttpRequest() // 1 - creating request object

xhr.open("GET", "http://student04.cse.nd.edu:51001/movies/32", true) // 2 - associates request attributes with xhr

xhr.onload = function(e) { // triggered when response is received
    // must be written before send
    console.log(xhr.responseText);
    label.innerHTML = xhr.responseText;
}

xhr.onerror = function(e) { // triggered when error response is received and must be before send
    console.error(xhr.statusText);
}

xhr.send(null) // last step - this actually makes the request

var label;
label = document.createElement("Label");
label.setAttribute("id", "theLabel");
//label.innerHTML = "waiting for response...";

document.body.appendChild(label);
