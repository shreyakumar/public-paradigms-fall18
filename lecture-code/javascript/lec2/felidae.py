class Felidae(object):
        """Base (abstract) class for Felidae."""
        @staticmethod
        def carnivorous():
                return True

        def can_roar(self):
                pass

        def has_spots(self):
                return False


class Pantherinae(Felidae):
        """Base (abstract) class for Pantherinae."""
        def can_roar():
                return True

class Lion(Pantherinae):
        """Class for Lion"""
        species_name = "P. leo"
        pic_url = "https://en.wikipedia.org/wiki/File:Lion_waiting_in_Namibia.jpg"

class Leopard(Pantherinae):
        """Class for Leopard"""
        species_name = "P. pardus"
        pic_url = "https://en.wikipedia.org/wiki/File:African_Leopard_5.JPG"

        def has_spots(self):
                return True

class Puma(Felidae):
        """Base (abstract) class for Puma."""
        pass

class Cougar(Puma):
        """Class for Cougar."""
        species_name = "P. concolor"
        pic_url = "https://en.wikipedia.org/wiki/File:Cougar_25.jpg"
