// js for js lecture 2

var tb = document.getElementById("theButton");
tb.onmouseup = addText;

var label = document.createElement("p");

function addText(){
    label.setAttribute("id", "theLabel");

    var textLabel = document.createTextNode("hlfflsnn");

    label.appendChild(textLabel);

    document.body.appendChild(label);
}

// ----------- inheritance

// --------- method 1
class Bird1{
    constructor(wingspan){
        this.wingspan = wingspan;
    }
}
console.log(Bird1.name);
console.log((new Bird1(5)).wingspan);

// --------- method 2
let Bird2 = class {
    constructor(wingspan){
        this.wingspan = wingspan;
    }
};

let Bird4 = class Bird3{
    constructor(wingspan){
        this.wingspan = wingspan;
    }
};

//class Duck3 extends Bird3{
    // Duck3 goes here
//}

// --------- method 3

var bird = {
    fly : function(){ alert("I'm flying!!");}
};

var sparrow = {
    name : "Jack",
    wingspan : 44,
};

var duck = {
    name : "Donald",
    wingspan : 10,
    quack : function(){alert("quack!");}
};

//sparrow.fly();
//alert(duck.name);

// prototypical inheritance
duck.__proto__ = bird;
duck.fly(); // this should work

var dodo = {
    name : "Momo",
    wingspan : 44,
    addClickHandler : function(handler, args){
        // todo
        document.getElementById("theButton").onmouseup = function(){ handler(args); } // inner function
    }
};

args = "this is my message";
function showTheMessage(args){
    alert(args);
}

dodo.addClickHandler(showTheMessage, args);
