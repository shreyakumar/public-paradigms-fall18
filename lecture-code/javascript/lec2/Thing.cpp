#include <iostream>

using namespace std;

class Thing{};

class Bird : public Thing {
public:
    void fly();

};

class Duck : public Bird {
public:
    void quack(){ cout << "I'm flying!" << endl; }

};

class Human : public Thing{
public:
    void quack();
};

int main(int argc, char const *argv[]) {
    Thing* donald = new Duck();
    ((Duck* )donald)->quack();
    return 0;
}
